"use strict";

export function createElt(type = "p", content = null, id = null, CSSClass = null) {
    let elt = document.createElement(type);
    if (content !== null) elt.textContent = content;
    if (id !== null) elt.setAttribute("id", id);
    if (CSSClass !== null) elt.className = CSSClass;

    return elt;
}

export function createImg(name) {
    let img = document.createElement("img");
    img.src = `./src/img/svg/${name}.svg`;
    img.alt = name;

    return img;
}

/**
 * Builds, displays then removes an informational pop-up
 *
 * @param string text
 * @param string skin
 */
export function displayPopup(text, skin = "dark") {
    let popupSection = document.getElementById('popup-section');
    let div = document.createElement('div');
    let p = document.createElement('p');
    let cross = document.createElement('div');
    let i = document.createElement('i');
    i.className = "fas fa-times";
    div.classList.add(skin);
    cross.classList.add("cross");
    p.innerHTML = text;
    cross.appendChild(i);
    div.appendChild(cross);
    div.appendChild(p);
    popupSection.appendChild(div);

    cross.addEventListener("click", () => {
        div.remove();
    });

    setTimeout(function () {
        div.remove();
    }, 10000);
}